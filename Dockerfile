FROM python:3.8.5-slim
RUN pwd
RUN ls
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apt-get clean && apt-get update \
    && apt-get -y install libpq-dev gcc \
    && pip install psycopg2


RUN apt-get -y install supervisor
RUN mkdir /src


COPY ./requirements.txt /src
RUN pip install --upgrade pip && pip install --no-cache-dir -r ./src/requirements.txt
RUN mkdir /var/log/supervisord
COPY . /src/test/
WORKDIR /src/test
# Install dependencies
RUN chmod +x /src/test/entrypoint.sh

RUN cp ./supervisord.conf /etc/supervisor/conf.d/
EXPOSE 8000
ENTRYPOINT ["sh","/src/test/entrypoint.sh"]


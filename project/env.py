import os

# show django error page. Should be false if not local env
DEBUG = os.environ.get("DEBUG", True)
# DB
AWS_REGION = os.environ.get("AWS_REGION", None)
USE_AWS_IAM = os.environ.get("USE_AWS_IAM", False)
RDS_DB_NAME = os.environ.get("RDS_DB_NAME", "test_pro")
RDS_USERNAME = os.environ.get("RDS_USERNAME", "test_pro")
RDS_PASSWORD = os.environ.get("RDS_PASSWORD", "test_pro")
# if USE_IAM_RDS is true, hostname cannot be a CNAME (CNAME is not suported with IAM RDS auth)
RDS_HOSTNAME = os.environ.get("RDS_HOSTNAME", "postgres")
RDS_PORT = os.environ.get("RDS_PORT", 5432)
# if USE_IAM_RDS is true, hostname cannot be a CNAME (CNAME is not suported with IAM RDS auth)
ENV = 'local'
# secret used as a salt for many things.
SECRET_KEY = os.environ.get(
    "SECRET_KEY", "asdfasdf#%6+^)n%s@5$t$m20"
)
EMAIL_HOST = 'smtp.mail.ru'
EMAIL_USE_TLS = True
EMAIL_PORT = 587
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
AWS_ACCESS_KEY_ID = os.environ.get(
    "AWS_ACCESS_KEY_ID", "asdfasf"
)
AWS_SECRET_ACCESS_KEY = os.environ.get(
    "AWS_ACCESS_KEY_ID", "asdfadsfa"
)

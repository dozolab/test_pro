from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static


admin.site.site_header = "Employee Book administration"
admin.site.site_title = "Employee Book administration"
admin.site.index_title = "Welcome to Employee Book Project"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('app.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

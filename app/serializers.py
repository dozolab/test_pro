from django.template.defaultfilters import floatformat
from django.contrib.humanize.templatetags.humanize import intcomma

from rest_framework import serializers
from .models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = '__all__'

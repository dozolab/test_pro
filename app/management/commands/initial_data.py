from django.core.management.base import BaseCommand
from django.db import transaction
from app.models import Department

from factories.utils import (
load_employees,
load_departments
)


class Command(BaseCommand):
    help = "Load initial data"

    def handle(self, *args, **options):
        if not Department.objects.all():
            print("Loading fake departments...")
            load_departments()
            print("Loading fake employees...")
            load_employees()
        else:
            pass

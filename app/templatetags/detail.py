from django import template

register = template.Library()


@register.inclusion_tag('detail.html')
def get_detail(department, is_initial=None):
    context = {}
    context.update({'department': department, 'child_departments': department.children.all(), 'is_initial':is_initial})
    return context

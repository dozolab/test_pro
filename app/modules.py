from django.conf import settings
from rest_framework import pagination
from rest_framework.response import Response


class PageNumberPaginationWithTotalPages(pagination.PageNumberPagination):

    def get_paginated_response(self, data):
        return Response({
            'page_number': self.page.number,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'previous_number': self.page.number-1,
            'next_number': self.page.number + 1,
            'count': self.page.paginator.count,
            'pagination_count_per_page': settings.REST_PAGE_SIZE,
            'total_pages': self.page.paginator.num_pages,
            'results': data
        })

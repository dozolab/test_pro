from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin
from app import models as app_models


@admin.register(app_models.Department)
class DepartmentAdmin(DraggableMPTTAdmin):
    list_display = ('id', 'tree_actions', 'indented_title')
    list_display_links = ('indented_title',)
    search_fields = ['title', 'id', ]

    def get_prepopulated_fields(self, request, obj=None):
        return {
            'slug': ('title',)
        }


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['id', 'full_name', 'division']

admin.site.register(app_models.Employee, EmployeeAdmin)

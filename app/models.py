from django.db import models
from django.apps import apps
from django.db import models
from django.db.models import Count
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from django.utils.translation import gettext_lazy as _


class Department(MPTTModel):
    title = models.CharField(max_length=250, null=True)
    slug = models.SlugField(max_length=255, null=True, blank=True, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, related_name='children')
    updated_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.title} -- {self.id}' if self.title else ""

    class Meta:
        verbose_name = 'Department'
        verbose_name_plural = 'Departments'

    def get_total_employees(self):
        return self.get_descendants(include_self=True).aggregate(
            total_products=Count('related_employees'))[
            'total_products']

    def get_all_employees(self):
        list_employees = []
        for i in self.get_descendants(include_self=True):
            list_employees.append(i.related_employees.all)
        return list_employees

    def ancestors(self):
        return super().get_ancestors(include_self=True)

    # def get_absolute_url(self):
    #     return reverse('app:product_list', args=[self.safe_translation_getter('slug'), ])

    @property
    def employees(self):
        employee = apps.get_model(app_label='app', model_name='Employee')
        return employee.objects.filter(division_id=self.id).order_by('pk')

    def detail_html(self):
        context = {}
        context.update({'department': self, 'child_departments': self.children.all()})
        return render_to_string('detail.html', context=context)


class Employee(models.Model):
    full_name = models.CharField(max_length=250)
    job_title = models.CharField(max_length=250)
    employment_date = models.DateTimeField(auto_now=True)
    division = TreeForeignKey(Department, on_delete=models.CASCADE, related_name='related_employees', null=True,
                              blank=True)
    amount_of_wages = models.FloatField()

    def __str__(self):
        return self.full_name

    # class Meta:
    #     ordering = ['position', ]

    def get_absolute_url(self):
        return reverse('app:product_list_on_brands', args=[self.id])

    def get_product_count(self):
        product = apps.get_model(app_label='store', model_name='Product')
        return product.objects.filter(available=True, brand_id=self.id).count()

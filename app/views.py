from django.shortcuts import render
from django.views import generic
from rest_framework import generics, renderers, response

from .models import Department, Employee
from rest_framework.pagination import PageNumberPagination
from app import serializers as restful_serializer


class HomeView(generic.TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Department.objects.filter(parent__isnull=True)
        return context


class EmployeeList(generics.ListAPIView):
    renderer_classes = [renderers.TemplateHTMLRenderer, renderers.JSONRenderer]
    serializer_class = restful_serializer.EmployeeSerializer
    # pagination_class = PageNumberPagination
    template_name = 'employee_list.html'
    queryset = Employee.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        queryparam = self.request.query_params
        department_id = queryparam.get('department_id', None)
        if department_id:
            queryset = queryset.filter(division__id=department_id)
        return queryset

    # def list(self, request, *args, **kwargs):
    #     if request.accepted_renderer.format == 'html':
    #         employees = Employee.objects.filter(division__id=request.pk)
    #         resp = response.Response({'emplo': brands, 'request': request})
    #     else:
    #         resp = super().list(request, *args, **kwargs)
    #     return resp

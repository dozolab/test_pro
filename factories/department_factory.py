import uuid
import factory.fuzzy
from faker import Faker
from app import models

fake = Faker()

# first, import a similar Provider or use the default one
from faker.providers import BaseProvider


# create new provider class
class DepartmentProvider(BaseProvider):
    def department(self) -> str:
        return 'department'


# then add new provider to faker instance
fake.add_provider(DepartmentProvider)


class DepartmentFactory(factory.django.DjangoModelFactory):
    """Facility factory."""

    title = factory.Faker("job")

    class Meta:
        model = models.Department

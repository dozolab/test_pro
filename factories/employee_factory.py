import random
import uuid
import factory.fuzzy
from faker import Faker
from app import models
from factories.department_factory import DepartmentFactory


class EmployeeFactory(factory.django.DjangoModelFactory):
    """Employee Factory."""

    full_name = factory.Faker("name")
    job_title = factory.Faker("job")
    division = factory.LazyFunction(
        lambda: random.choice(models.Department.objects.all())
    )
    employment_date = factory.Faker(
        "date_time_this_century", before_now=True, after_now=False
    )
    amount_of_wages = factory.fuzzy.FuzzyChoice([120_000.50, 130_000.50, 140_000.5, 155_000.5, 200_000.500])

    class Meta:
        model = models.Employee

import time

from app.models import Department
from .department_factory import DepartmentFactory
from .employee_factory import EmployeeFactory
import multiprocessing
import concurrent.futures


def load_departments():
    for i in range(1, 6):
        department = DepartmentFactory()
        department.save()
    last_department = Department.objects.last()
    initial_id = last_department.id - 4
    for i in range(1, 21):
        department = DepartmentFactory()
        department.parent = Department.objects.get(id=initial_id)
        initial_id += 1
        department.save()


def create_employee(count):
    employee = EmployeeFactory()
    employee.save()
    return employee


def load_employees():
    start = time.perf_counter()
    with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
        results = executor.map(create_employee, range(1, 50001))
        for result in results:
            print(result)
    finish = time.perf_counter()
    print(f'Created 50 000 employees in {round(finish - start)} seconds ')

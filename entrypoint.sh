#!/bin/sh
set -e
python manage.py makemigrations --merge --no-input
python manage.py migrate
python manage.py collectstatic||echo
python manage.py initial_data
DJANGO_SUPERUSER_PASSWORD=123 ./manage.py createsuperuser     --no-input     --username=test     --email=my_user@domain.com||echo
#/usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf
gunicorn project.wsgi:application --bind 0.0.0.0:8000
#exit


